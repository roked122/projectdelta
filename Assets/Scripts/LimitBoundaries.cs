﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitBoundaries : MonoBehaviour
{
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -1.8f, 1.8f),
            Mathf.Clamp(transform.position.y, -4.5f, 4.5f), transform.position.z);
    }
}
