﻿using UnityEngine;
using UnityEngine.Audio;

public class VolumeControl : MonoBehaviour
{
    public AudioMixer mixer;

    public void SetLevel (float slideValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(slideValue) * 20);
    }
}
