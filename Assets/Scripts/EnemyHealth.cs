﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int HP = 15;
    bool isDestroyed = false;

    public bool IsDestroyed()
    {
        return isDestroyed;
    }

    public void maganeHealth(int demage)
    {
        HP -= demage;
        if (HP < 1)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        isDestroyed = true;
    }
}
