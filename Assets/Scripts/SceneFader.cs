﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneFader : MonoBehaviour
{
    public Image image;
    public AnimationCurve curve;
    public static float deltaTime;
    private static float _lastframetime;

    private void Start()
    {
        Time.timeScale = 1f;
        _lastframetime = Time.realtimeSinceStartup;
        StartCoroutine(FadeIn());
    }
    void Update()
    {
        deltaTime = Time.realtimeSinceStartup - _lastframetime;
        _lastframetime = Time.realtimeSinceStartup;
    }

    public void FadeTo(string scene)
    {
        StartCoroutine(FadeOut(scene));
    }

    public void FadeToIndex(int index)
    {
        StartCoroutine(FadeOutIndex(index));
    }

    IEnumerator FadeIn()
    {
        float time = 1f;

        while(time > 0)
        {
            time -= deltaTime;
            float alpha = curve.Evaluate(time);
            image.color = new Color(0f, 0f, 0f, alpha);
            yield return 0;
        }
    }

    IEnumerator FadeOut(string scene)
    {
        float time = 0f;

        while (time < 1f)
        {
            time += deltaTime;
            float alpha = curve.Evaluate(time);
            image.color = new Color(0f, 0f, 0f, alpha);
            yield return 0;
        }

        SceneManager.LoadScene(scene);
    }

    IEnumerator FadeOutIndex(int index)
    {
        float time = 0f;

        while (time < 1f)
        {
            time += deltaTime;
            float alpha = curve.Evaluate(time);
            image.color = new Color(0f, 0f, 0f, alpha);
            yield return 0;
        }

        SceneManager.LoadScene(index);
    }
}
