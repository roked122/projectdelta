﻿using TMPro;
using UnityEngine;

public class ScoreHendler : MonoBehaviour
{
    public TMP_Text scorePoint;
    public TMP_Text finalScoreLose; 
    public TMP_Text finalScoreWin;

    private int score;

    public void FinalScore()
    {
        finalScoreWin.text = score.ToString();
        finalScoreLose.text = score.ToString();
    }
    public void AddPoints(int points)
    {
        score += points;
        scorePoint.text = score.ToString();
    }
}
