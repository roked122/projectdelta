﻿using System;
using UnityEngine;

[DisallowMultipleComponent]
public class PlayerBullet : MonoBehaviour
{
    public float speed = 10f;
    public Rigidbody2D rb;
    public int demage = 3;
    public ParticleSystem hitFXMeteor;
    public ParticleSystem hitFXEnemy;
    public AudioClip hitSEMeteor;
    public AudioClip hitSEEnemy;

    AudioSource audioSource;

    void Start()
    {
        audioSource = GameObject.Find("AudioHendler").GetComponent<AudioSource>();
        rb.velocity = transform.up * speed;
        FireSound();
    }

    private void FireSound()
    {
        GetComponent<AudioSource>().Play();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Boundary")
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Meteroite")
        {
            OnHitMeteor();
            Destroy(gameObject);
            MeteoriteHealth target = collision.GetComponent<MeteoriteHealth>();
            if (target == null) return;
            target.maganeHealth(demage);
        }
        else if (collision.tag == "Enemy")
        {
            OnHitShip();
            Destroy(gameObject);
            EnemyHealth target = collision.GetComponent<EnemyHealth>();
            if (target == null) return;
            target.maganeHealth(demage);
        }
    }

    private void OnHitMeteor()
    {
        var fx = Instantiate(hitFXMeteor, transform.position, Quaternion.identity);
        fx.Play();
        Destroy(fx.gameObject, 0.3f);
        audioSource.PlayOneShot(hitSEMeteor, 0.35f);
    }

    private void OnHitShip()
    {
        var fx = Instantiate(hitFXEnemy, transform.position, Quaternion.identity);
        fx.Play();
        Destroy(fx.gameObject, 0.3f);
        audioSource.PlayOneShot(hitSEEnemy, 0.35f);
    }
}
