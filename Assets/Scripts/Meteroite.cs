﻿using UnityEngine;
using UnityEngine.UI;

public class Meteroite : MonoBehaviour
{
    public float speed = 10f;
    public int pointsPerKill = 1;
    public Rigidbody2D rb;   
    public AudioClip boomSE;
    public ParticleSystem boomFX;

    MeteoriteHealth health;
    AudioSource audioSource;

    //feature release
    private int demage = 25;

    void Start()
    {
        audioSource = GameObject.Find("AudioHendler").GetComponent<AudioSource>();
        health = GetComponent<MeteoriteHealth>();
        rb.velocity = transform.up * speed;
    }

    private void Update()
    {
        if (health.IsDestroyed())
        {
            SoundAndFXOnBoom();
            Destroy(gameObject);
            AddPointsToPlayer();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        HandleHitsAndSceneEnd(collision);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        HandlePlayerHit(collision);
    }

    private void HandleHitsAndSceneEnd(Collider2D collision)
    {
        if (collision.tag == "Boundary")
        {
            return;
        }
        else if (collision.tag == "Bullet")
        {
            if (health.IsDestroyed())
            {
                SoundAndFXOnBoom();
                Destroy(gameObject);
                AddPointsToPlayer();
            }
        }
        else if (collision.tag == "SceneEnd")
        {
            Destroy(gameObject);
        }
    }

    private void AddPointsToPlayer()
    {
        ScoreHendler score = GameObject.Find("EventHendler").GetComponent<ScoreHendler>();
        score.AddPoints(pointsPerKill);
    }

    private void HandlePlayerHit(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            SoundAndFXOnBoom();
            Destroy(gameObject);
            ShipHealth target = collision.GetComponent<ShipHealth>();
            if (target == null) return;
            target.menageHealth(demage);
        }
    }

    private void SoundAndFXOnBoom()
    {
        var fx = Instantiate(boomFX, transform.position, Quaternion.identity);
        fx.Play();
        Destroy(fx.gameObject, 0.3f);
        audioSource.PlayOneShot(boomSE, 0.55f);
    }

}
