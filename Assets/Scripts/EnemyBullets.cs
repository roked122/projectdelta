﻿using UnityEngine;

[DisallowMultipleComponent]
public class EnemyBullets : MonoBehaviour
{
    public float speed = 10f;
    public Rigidbody2D rb;
    public int demage = 25;

    void Start()
    {
        rb.velocity = -transform.up * speed;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Boundary")
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
            ShipHealth target = collision.GetComponent<ShipHealth>();
            if (target == null) return;
            target.menageHealth(demage);
        }
    }
}
