﻿using UnityEngine;

public class Ship : MonoBehaviour
{
    ShipHealth health;
    public AudioClip hitSE;
    public AudioClip boomSE;
    public ParticleSystem hitFX;
    public ParticleSystem boomFX;

    AudioSource audioSource;

    void Start()
    {
        audioSource = GameObject.Find("AudioHendler").GetComponent<AudioSource>();
        health = GetComponent<ShipHealth>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Meteroite")
        {
            if (health.IsDead())
            {
                OnDead();
            }

            OnHit();
            //TODO Efects
        }

        if (collision.tag == "EnemyBullet")
        {
            if (health.IsDead())
            {
                OnDead();
            }
            OnHit();
        }

        if (collision.tag == "Enemy")
        {
            if (health.IsDead())
            {
                OnDead();
            }
            OnHit();
        }
    }

    private void OnDead()
    {
        SoundAndFXOnBoom();
        Destroy(gameObject);
        //set the score
        ScoreHendler score = GameObject.Find("EventHendler").GetComponent<ScoreHendler>();
        score.FinalScore();
        //Open game over menu
        GameOverMenu menu = GameObject.Find("StandartGameLevelCanvas").GetComponent<GameOverMenu>();
        menu.GameOver();
    }

    private void SoundAndFXOnBoom()
    {
        var fx = Instantiate(boomFX, transform.position, Quaternion.identity);
        fx.Play();
        Destroy(fx.gameObject, 0.3f);
        audioSource.PlayOneShot(boomSE, 0.55f);
    }

    private void OnHit()
    {
        var fx = Instantiate(hitFX, transform.position, Quaternion.identity);
        fx.Play();
        Destroy(fx.gameObject, 0.3f);
        audioSource.PlayOneShot(hitSE, 0.35f);
    }
}
