﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    public Transform firepoint;
    public GameObject bullet;
    public float fireRate = 1f;

    private float timer;

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            Instantiate(bullet, firepoint.position, firepoint.rotation);

            timer = fireRate;
        }
    }
}
