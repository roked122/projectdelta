﻿using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 0.5f;

    void Start()
    {

    }

    void Update()
    {
        Vector2 offset = new Vector2(0, Time.time * rotationSpeed);

        GetComponent<Renderer>().material.mainTextureOffset = offset;
    }
}
