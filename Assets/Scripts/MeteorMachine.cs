﻿using System.Collections.Generic;
using UnityEngine;

public class MeteorMachine : MonoBehaviour
{
    public List<Transform> dropPoint;
    public List<GameObject> meteor;
    public float spawnTime = 2f;
    public float RepeatTime = 1.5f;

    private int lastPickedPoint;

    private void Start()
    {
        InvokeRepeating("Drop", spawnTime, RepeatTime);
    }

    public void StopInvoke()
    {
        CancelInvoke("Drop");
    }

    private void Drop()
    {
        PickAPoint();
    }

    private void PickAPoint()
    {
        var dropPointIndex = Random.Range(0, dropPoint.Count);

        if (dropPointIndex == lastPickedPoint && dropPointIndex == dropPoint.Count - 1)
        {
            dropPointIndex = Random.Range(0, dropPoint.Count - 1);
        }
        else if (dropPointIndex == lastPickedPoint && dropPointIndex == dropPoint.Count - (dropPoint.Count - 1))
        {
            dropPointIndex = Random.Range(1, dropPoint.Count);
        }
        else if(dropPointIndex == lastPickedPoint)
        {
            dropPointIndex = Random.Range(lastPickedPoint, dropPoint.Count);
        }

        if (dropPointIndex != lastPickedPoint)
        {
            Instantiate(meteor[Random.Range(0, meteor.Count)],
                dropPoint[dropPointIndex].position,
                dropPoint[dropPointIndex].rotation);
            lastPickedPoint = dropPointIndex;
        }
    }
}
