﻿using System;
using System.Collections;
using UnityEngine;

public class LevelTime : MonoBehaviour
{
    public string nextLevel = "Level02";
    public int levelToUnlock = 2;
    private float speed = 3;

    void Start()
    {
        StartCoroutine(reloadTimer(30));
    }


    IEnumerator reloadTimer(float reloadTimeInSeconds)
    {
        float counter = 0;

        while (counter < reloadTimeInSeconds)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Load win menu
        EndLevel();

    }

    private void EndLevel()
    {
        PlayerPrefs.SetInt("LevelReached", levelToUnlock);

        StopSpawning();
        StartCoroutine(CheckForMeteroites());
    }

    IEnumerator CheckForMeteroites()
    {
        while (GameObject.FindWithTag("Meteroite") != null)
        {
            yield return null;
        }

        //TODO one more second
        DisableMvmAndShoot();
        MoveShip();
        Invoke("LoadWinmenu", 3f);
    }

    private void StopSpawning()
    {
        MeteorMachine meteorMachine = GameObject.Find("Obstacles").GetComponent<MeteorMachine>();
        meteorMachine.StopInvoke();
    }

    private void MoveShip()
    {
        Rigidbody2D rb = GameObject.Find("PlayerShip 1.0").GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;
    }

    private void DisableMvmAndShoot()
    {
        DragToMove dragToMove = GameObject.Find("PlayerShip 1.0").GetComponent<DragToMove>();
        PlayerWeapon playerWeapon = GameObject.Find("PlayerShip 1.0").GetComponent<PlayerWeapon>();
        LimitBoundaries limitBoundaries = GameObject.Find("PlayerShip 1.0").GetComponent<LimitBoundaries>();
        limitBoundaries.enabled = false;
        dragToMove.enabled = false;
        playerWeapon.enabled = false;
    }

    private void LoadWinmenu()
    {
        WinMenu menu = GameObject.Find("StandartGameLevelCanvas").GetComponent<WinMenu>();
        menu.Win();
    }
}
