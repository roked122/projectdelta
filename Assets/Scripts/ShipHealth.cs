﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipHealth : MonoBehaviour
{
    public int HP = 100;
    bool isDead = false;
    public List<GameObject> hearts;
    private int count;

    public bool IsDead()
    {
        return isDead;
    }

    public void menageHealth(int demage)
    {
        //HP -= demage;
        ManageLifeNum();
        /*if (HP < 1)
        {
            Dead();
        }*/
    }

    private void ManageLifeNum()
    {
        count = hearts.Count-1;
        hearts[count].SetActive(false);
        hearts.RemoveAt(count);
        count--;
        if (hearts.Count == 0)
        {
            Dead();
        }
    }

    private void Dead()
    {
        isDead = true;
    }
}
