﻿using System.Collections.Generic;
using UnityEngine;

public class EnemtShipMachine : MonoBehaviour
{
    public List<Transform> dropPoint;
    public List<GameObject> enemy;
    public float spawnTime = 2f;

    private void Start()
    {
        InvokeRepeating("Create", spawnTime, spawnTime);
    }

    private void Create()
    {
        var dropPointIndex = Random.Range(0, dropPoint.Count);

        Instantiate(enemy[Random.Range(0, enemy.Count)],
            dropPoint[dropPointIndex].position,
            dropPoint[dropPointIndex].rotation);
    }
}
