﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    public Transform firepoint;
    public GameObject bullet;
    public float fireRate = 1f;

    private float timer;
    void Update()
    {
        timer -= Time.deltaTime;
        if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Stationary 
            || Input.GetTouch(0).phase == TouchPhase.Moved) && timer <= 0f)
        {
            Instantiate(bullet, firepoint.position, firepoint.rotation);

            timer = fireRate;
        }

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            return;
            //TODO slow/freeze game
        }
    }


}
