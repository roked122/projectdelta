﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour
{
    public GameObject winMenuUI;
    public SceneFader sceneFader;

    // Start is called before the first frame update
    public void Win()
    {
        winMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void NextLevel()
    {
        sceneFader.FadeToIndex(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadLevels()
    {
        sceneFader.FadeTo("Levels");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
