﻿using UnityEngine;

public class MeteoriteHealth : MonoBehaviour
{
    public int HP = 6;
    bool isDestroyed = false;

    public bool IsDestroyed()
    {
        return isDestroyed;
    }

    public void maganeHealth(int demage)
    {
        HP -= demage;
        if(HP < 1)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        isDestroyed = true;
    }
}
