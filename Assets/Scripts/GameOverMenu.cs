﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public GameObject gameOverMenuUI;
    public SceneFader sceneFader;

    public void GameOver()
    {
        gameOverMenuUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void PlayAgain()
    {
        Scene scene = SceneManager.GetActiveScene();
        sceneFader.FadeTo(scene.name);
    }

    public void LoadMenu()
    {
        sceneFader.FadeTo("Menu");
    }

    //TODO probably remove
    public void QuitGame()
    {
        Application.Quit();
    }

}
